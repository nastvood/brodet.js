// brodet.js 0.0.3
// (c) 2016 Agapiy Denis

(function(){
    // Constants
    var BROWSER_UNKNOWN = 'unknown';
    var BROWSER_YANDEX = 'Yandex';
    var BROWSER_OPERA = 'Opera';
    var BROWSER_FIREFOX = 'Firefox';
    var BROWSER_MAXTHON = 'Maxthon';
    var BROWSER_IE = 'Internet Explorer';
    var BROWSER_CHROME = 'Chrome';
    var BROWSER_SAFARI = 'Safari';
	var BROWSER_EDGE = 'Edge';

    var Brodet = function(agent){
        var self = this;
		
		self.isBrowserEdge = function(){
            var res, subver;
            var posMatch = self.userAgent.indexOf('Edge');
            if (posMatch > 0){
                res = self.userAgent.substr(posMatch).split('/');
                if (res.length < 2){
                    return false;
                }
                self.version = res[1].substr(0, res[1].length - 1);
                subver = self.version.split('.');

                self.browser = BROWSER_EDGE;
                self.majorVersion = parseInt(subver[0], 10);
                self.minorVersion = parseInt(subver[1], 10);

                return true;
            }

            return false;
        };

        self.isBrowserIE = function(){
            var res, subver;
            var posMatch = self.userAgent.indexOf('MSIE');
            if (posMatch > 0){
                res = self.userAgent.substr(posMatch).split(' ');
                if (res.length < 2){
                    return false;
                }
                self.version = res[1].substr(0, res[1].length-1);
                subver=self.version.split('.');

                self.browser = BROWSER_IE;
                self.majorVersion = parseInt(subver[0], 10);
                self.minorVersion = parseInt(subver[1], 10);

                return true;
            }else{
                posMatch = self.userAgent.indexOf('rv:');
                if ((posMatch > 0) && (self.userAgent.indexOf('like Gecko') > 0)){
                    res = self.userAgent.substr(posMatch).split(':');
                    if (res.length < 2){
                        return false;
                    }
                    res = res[1].split(')');
                    self.version = res[0];
                    subver = self.version.split('.');

                    self.browser = BROWSER_IE;
                    self.majorVersion = parseInt(subver[0], 10);
                    self.minorVersion = parseInt(subver[1], 10);

                    return true;
                }
            }

            return false;
        };

        self.isBrowserYandex = function(){
            var posMatch = self.userAgent.indexOf('YaBrowser');
            if (posMatch >= 0){
                var res = self.userAgent.substr(posMatch).split('/');
                if (res.length < 2){
                    return;
                }
                var ver = res[1].split(" ");

                var subver = ver[0].split(".");
                if (subver.length > 1){
                    self.minorVersion = parseInt(subver[1], 10);
                }
                self.browser = BROWSER_YANDEX;
                self.version = ver[0];
                self.majorVersion = parseInt(subver[0], 10);
            }

            return false;
        };

        self.isBrowserOpera = function(){
            var ver, subver;
            var posMatch = self.userAgent.indexOf('OPR');
            if (posMatch >= 0){
                var res = self.userAgent.substr(posMatch).split('/');
                if (res.length < 2){
                    return false;
                }
                ver=res[1].split(' ');

                subver = ver[0].split('.');
                if (subver.length > 1){
                    self.minorVersion = parseInt(subver[1], 10);
                }
                self.browser = BROWSER_OPERA;
                self.version = ver[0];
                self.majorVersion = parseInt(subver[0], 10);

                return true;
            }else{
                posMatch = self.userAgent.indexOf('Opera');
                if (posMatch >= 0){
                    posMatch = self.userAgent.indexOf('Version');

                    ver = self.userAgent.substr(posMatch).split('/');
                    if (ver.length < 2){
                        return false;
                    }
                    subver = ver[1].split(".");
                    if (subver.length > 1){
                        self.minorVersion = parseInt(subver[1], 10);
                    }
                    self.browser = BROWSER_OPERA;
                    self.version = ver[1];
                    self.majorVersion = parseInt(subver[0], 10);

                    return true;
                }
            }

            return false;
        };

        self.isBrowserFirefox = function(){
            var posMatch = self.userAgent.indexOf('Firefox');
            if (posMatch >= 0){
                var res = self.userAgent.substr(posMatch).split('/');
                if (res.length < 2){
                    return false;
                }
                var ver = res[1].split(' ');

                var subver = ver[0].split('.');
                if (subver.length > 1){
                    self.minorVersion = parseInt(subver[1],10);
                }
                self.browser = BROWSER_FIREFOX;
                self.version = ver[0];
                self.majorVersion = parseInt(subver[0], 10);

                return true;
            }

            return false;
        };

        self.isBrowserMaxthon = function(){
            var posMatch = self.userAgent.indexOf('Maxthon');
            if (posMatch >= 0){
                var res = self.userAgent.substr(posMatch).split('/');
                if (res.length < 2){
                    return false;
                }
                var ver = res[1].split(' ');
                var subver = ver[0].split('.');
                if (subver.length > 1){
                    self.minorVersion = parseInt(subver[1], 10);
                }
                self.browser = BROWSER_MAXTHON;
                self.version = ver[0];
                self.majorVersion = parseInt(subver[0], 10);

                return true;
            }

            return false;
        };

        self.isBrowserChrome = function(){
            if (self.userAgent.indexOf('YaBrowser') > 0){
                return false;
            }
			
			if (self.userAgent.indexOf('Edge') > 0){
                return false;
            }

            var posMatch = self.userAgent.indexOf('Chrome');
            if ((posMatch > 0) && (self.userAgent.indexOf('Safari') > 0)){
                var res = self.userAgent.substr(posMatch).split('/');
                if (res.length < 2){
                    return false;
                }
                var ver = res[1].split(' ');
                var subver = ver[0].split('.');
                if (subver.length > 1){
                    self.minorVersion = parseInt(subver[1], 10);
                }
                self.browser = BROWSER_CHROME;
                self.version = ver[0];
                self.majorVersion = parseInt(subver[0], 10);

                return true;
            }

            return false;
        };

        self.isBrowserSafari = function(){
            var posMatch = self.userAgent.indexOf('Safari');
            if (posMatch >= 0){
                posMatch = self.userAgent.indexOf('Version');
                if (posMatch < 0){
                    return false;
                }

                var ver = self.userAgent.substr(posMatch+'Version'.length + 1).split(' ');
                if (ver.length < 2){
                    return false;
                }
                var subver = ver[0].split('.');
                if (subver.length > 1){
                    self.minorVersion = parseInt(subver[1], 10);
                }
                self.browser = BROWSER_SAFARI;
                self.version = ver[0];
                self.majorVersion = parseInt(subver[0], 10);

                return true;
            }
            return false;
        };

        self.toString = function(){
            return self.browser + '/' + self.version;
        };

        self.init = function(agent){
            self.userAgent = agent || navigator.userAgent;
            self.browser = BROWSER_UNKNOWN;
            self.version = BROWSER_UNKNOWN;
            self.majorVersion = 0;
            self.minorVersion = 0;

            for(var key in self){
                if (key.indexOf('isBrowser') === 0){
                    if (self[key]()){
                        return true;
                    }
                }
            }
        };

        self.init(agent);
    };

    Brodet.prototype.detect = function(agent){
        return new Brodet(agent);
    };

    if (typeof window === 'object' && typeof window.document === 'object') {
        window.brodet = new Brodet();
    }
})();


