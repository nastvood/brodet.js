# brodet.js

Browser detect

## Usage
Brodet instantiates itself onto the window.

    <script src="brodet.min.js"></script>
    <script>
        console.log(brodet.browser);
        console.log(brodet.version);
        console.log(brodet.majorVersion);
        console.log(brodet.minorVersion);
        console.log(brodet.isBrowserChrome());
    </script>