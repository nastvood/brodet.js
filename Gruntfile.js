module.exports=function(grunt){
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '// <%= pkg.name %>.js\n// (c) <%= grunt.template.today("yyyy")%> <%= pkg.author %>  \n '
            },
            build: {
                src: '<%= pkg.name %>.js',
                dest: '<%= pkg.name %>.min.js'
            }
        },
        jshint:{
            options:{
                curly:true,
                eqeqeq:true,
                es3:true,
                latedef:true,
                noempty:true,
                unused:true
            },
            all:['<%= pkg.name %>.js']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.registerTask('default',['jshint','uglify']);
    grunt.registerTask('check',['jshint']);
}